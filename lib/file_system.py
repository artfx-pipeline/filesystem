# -*- coding: utf-8 -*-

"""

File system helper
for windows / linux / mac

"""

import os
import math
import glob

import six

from template import path_to_dict, dict_to_path
from file_info import get_size, get_create_date


def get_childs(data):
    """
    Get childs return the next conform directories from data
    Given data can have * for glob search
    :param data: Data from a valid path (can be a valid conform path)
    :return: a generator of data (dictionary)
    """
    path = dict_to_path(data) if isinstance(data, dict) else data
    if not path:
        raise ValueError("Given data can't be convert into path")
    if not os.path.exists(path):
        raise ValueError("Invalid path : {0}".format(path))
    for path in glob.glob(path):
        for directory in os.listdir(path):
            data = path_to_dict(os.path.join(path, directory))
            if not data:
                continue
            if "ext" in data:
                data["size"] = get_size(data["path"])
                data["create_at"] = get_create_date(data["path"]) 
            yield data


def get_directories(path, full_path=True, recursive=False):
    """
    Get the list of folders in the path
    See display_directories to see how to use return
    :param path: path to return child folder
    :param full_path: if true return full path else folder name
    :param recursive: To get all the folder tree
    (return list with key foldername and value list of forlder child)
    :return: A list (with recursive a list of dict)
    """
    if not os.path.exists(path):
        raise ValueError("{path} don't exist.".format(path=path))
    for directory in os.listdir(path):
        full_path_dir = os.path.join(path, directory)
        if os.path.isdir(full_path_dir):
            if full_path:
                directory = full_path_dir
            if recursive:
                yield {directory: get_directories(full_path_dir, full_path, recursive)}
            else:
                yield directory


def get_files(path, full_path=True, recursive=False):
    """
    Get the list of files in the path
    See display_files to see how to use return
    :param path: path to return child files
    :param full_path: if true return full path else only files name
    :param recursive: To get all the files tree
    :return: A generator
    """
    if not os.path.exists(path):
        raise ValueError("{path} don't exist.".format(path=path))
    # directory_content can be file or directory (file return/dir recursive continue)
    for directory_content in os.listdir(path):
        full_path_dir = os.path.join(path, directory_content)
        if full_path:
            directory_content = full_path_dir
        if os.path.isfile(full_path_dir):
            yield directory_content
        elif recursive and os.path.isdir(full_path_dir):
            yield get_files(full_path_dir, full_path, recursive)


def crate_file(data, content, option="w+"):
    """
    Create a path file with the content
    :param data: complete data
    :param content: Content in the path
    :param option: w for write, a for append (default w+) see python open()
    """
    path = dict_to_path(data) if isinstance(data, dict) else data
    file = open(os.path.normpath(path), option)
    file.write(content)
    file.close()


if __name__ == '__main__':
    from pprint import pprint

    data_test = {
        "project": "pipe",
        "entity": "s",
        "type": "scenes",
        "seq": "010",
        "shot": "010",
        "task": "layout",
        "subtask": "main",
        "state": "w"
    }
    for child in get_childs(data_test):
        pprint(child)
