# -*- coding: utf-8 -*-

"""

A simple file info tool

"""

from datetime import datetime


def get_size(file):
    """
    Get the file size
    :param file: File we want the size
    :return: str size (with MB, GB, ...)
    """
    return convert_size(os.path.getsize(file))


def get_create_date(file):
    """
    Get the created date of a file
    :param file: File we want the created date
    :return: str date (dd/mm/yyyy hh:mm)
    """
    time_span = os.path.getctime(file)
    return str(datetime.fromtimestamp(time_span).strftime('%d/%m/%Y %H:%M')) or "00/00/0000 00:00"


def convert_size(size_bytes):
    """ Utility fonction for file size """
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])