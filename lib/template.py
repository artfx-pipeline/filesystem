# -*- coding: utf-8 -*-

"""

File system template

"""

import os
import six
import lucidity
from lucidity import Template
from template_conf import path_templates, path_templates_reference, path_mapping, path_defaults, get_datatype, force_path

resolvers = {path_templates_reference: Template(path_templates_reference, path_templates.get(path_templates_reference))}


def path_to_dict(path):
    """
    Change the path to a dictonary (with templates)
    :path: the path to transform
    :return: a dictionary with the key/value from path
    """
    path = path.replace(os.sep, '/')

    templates = []
    for name, pattern in six.iteritems(path_templates):
        template = Template(name, pattern,
                            anchor=lucidity.Template.ANCHOR_BOTH,
                            default_placeholder_expression='[^/]*',  # needed to use '#' for a path
                            duplicate_placeholder_mode=lucidity.Template.STRICT)
        template.template_resolver = resolvers

        templates.append(template)

    try:
        data, template = lucidity.parse(path, templates)
    except Exception as e:
        return None

    if not data:
        return None
    for key, value in six.iteritems(data):
        if path_mapping.get(key):
            value = path_mapping.get(key).get(value, value)
            data[key] = value
    data["path"] = path
    return data  # need the name ?


def dict_to_path(data):
    """
    Change the dictonary to a path (with templates)
    :data: the data dictonary to transform
    :return: a path
    """
    if not data:
        raise Exception('[dict_to_path] Data is empty')

    data = data.copy()

    # setting forced
    for key, value in six.iteritems(force_path):
        if key in data.keys() and value[0] not in data.keys():
            data[value[0]] = value[1]

    # setting defaults / convert
    for key in data.keys():
        if not data.get(key) and path_defaults.get(key):
            data[key] = path_defaults.get(key)

    # reverse path mapping
    for key, value in six.iteritems(data):
        # if key == "project":
        #     for project, project_sid in path_mapping[key].items():
        #         if os.path.normcase(project_sid) == value:
        #             data[key] = project
        if value and path_mapping.get(key):
            for path_val, data_val in path_mapping[key].items():
                if os.path.normcase(data_val) == value:
                    data[key] = path_val

    subtype = get_datatype(data)

    # debug('sidtype: {}'.format(subtype))

    pattern = path_templates.get(subtype)

    # debug('pattern: {}'.format(pattern))

    if not pattern:
        raise Exception('[dict_to_path] Unable to find pattern for data: "{}" \nGiven data: "{}"'.format(subtype, data))

    template = Template(subtype, pattern)
    template.template_resolver = resolvers

    # debug('template: {}'.format(template))

    if not template:
        raise Exception('[dict_to_path] Unable to find template')

    # adding template specific defaults
    for key in template.keys():
        if key not in data.keys() and path_defaults.get(key):
            data[key] = path_defaults.get(key)

    # debug('data after path_defaults: {}'.format(data))
    path = template.format(data)

    # debug('found: {}'.format(path))

    return path
